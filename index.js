// console.log("Hello World!");

// Exponent Operator - we use "**" for exponents

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2); // This is before ES6
console.log(secondNum);

const thirdNum = 5 ** 5;
console.log(thirdNum);

// Template Literals
/*
	Allows us to write strings without using the concatenation operato (+).
*/

let name = "George";

// Pre-Template Literal String
// Using singe quote ('')

let message = "Hello " + name + ". Welcome to programming!";
console.log("Message without template literal:" + message);

// String using Template Literal
// Uses the backticks (``)

message = `Hello ${name}. Welcome to programming!`;
console.log(`Message with template literal: ${message}.`);

let anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with a solution of ${firstNum}.
`;

console.log(anotherMessage);

anotherMessage =
  "\n " +
  name +
  " attended a math competition. \n He won it by solving the problem 8 ** 2 with a solution of " +
  firstNum +
  ". \n";
console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;
console.log(`The interest of your savings is: ${principal * interestRate}`);

// Array Destructuring
/*
	Allows us to unpack elements from an array into distinct variable, Allows us to name array elements with variables instead of index numbers

	Syntax:
		let/const [variableName, variableName, variableName] = arrayName;

*/

const fullName = ["Joe", "Dela", "Cruz"];

// Pre-Array Destructre
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(
  `Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`
);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(
  `Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`
);

// Object Destructuring
/*
	Allows us to unpack properties of objects into distinct variable. Shortens the syntax for accessing properties from objects.

	Syntax:
		let/const { propertyName, propertyName, propertyName} = objectName;
*/

const person = {
  givenName: "Jane",
  maidenName: "Dela",
  familyName: "Cruz",
};

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

function getFullNameA(givenName, maidenName, familyName) {
  console.log(`Hi! My name is ${givenName} ${maidenName} ${familyName}.`);
}

getFullNameA(person.givenName, person.maidenName, person.familyName);

// Object Destructuring
const { maidenName, givenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

function getFullNameB({ givenName, maidenName, familyName }) {
  console.log(`Hi! My name is ${givenName} ${maidenName} ${familyName}`);
}

getFullNameB(person);

/* Passing the values using the destructured properties of the object
function getFullNameB(givenName, maidenName, familyName) {
  console.log(`Hi! My name is ${givenName} ${maidenName} ${familyName}`);

  getFullNameB(givenName, maidenName, familyName);

}*/

// PRE-ARROw FUNCTION AND ARROW FUNCTION

// Pre-Arrow Function
/* 
    Syntax:
        function functionName(paramA, paramB) {
            statement
        }
*/

function printWholeName(firstName, middleName, lastName) {
  console.log(firstName + " " + middleName + " " + lastName);
}

printWholeName("John", "D", "Smith");

// Arrow Function

/* 
    Syntax:
        let/const variableName = (paramA, paramB) => {
            statement
        }

*/

const printFullName1 = (firstName, middleName, lastName) => {
  console.log(`${firstName} ${middleName} ${lastName}`);
};

printFullName1("Aljohn", "U", "Percano");

// Functions
const students = ["Ronie", "Aljohn", "Neil"];

// Pre-Arrow Function
students.forEach(function (student) {
  console.log(`${student} is a student from Batch-224`);
});

console.log(" ");

// Arrow Function
students.forEach((student) => {
  console.log(`${student} is a student from Batch-224`);
});

// Implicit Return Statement

// Pre-Arrow Function
function add(x, y) {
  return x + y;
}

let total = add(12, 15);
console.log(total);

// Arrow Function
const addition = (x, y) => x + y;

/* Long method
const addition = (x, y) => {
    return x + y;
}
*/

let resultOfAddition = addition(12, 15);
console.log(resultOfAddition);

// Default Funciton Argument Value

const greet = (name = "User") => {
  return `Good morning, ${name}`;
};

console.log(greet());
console.log(greet("Ariana"));

// Class - Based Object Blueprints
/* 
    Allows the creation/instantiation of objects using classes as blueprints

    Syntax:
        class className {
            constructor (objectPropertyA, objectPropertyB ) {
                this.objectPropertyA = objectPropertyA
                this.objectPropertyB = objectPropertyB
            };
        };

*/

// Template/Blueprint
class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);
